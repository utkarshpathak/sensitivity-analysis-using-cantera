import sys
import numpy as np
import matplotlib.pyplot as plt
import cantera as ct

gas = ct.Solution('cs1.cti')
temp = 680.0
pres = 15.0*100000

gas.TPX = temp, pres, 'NH3:0.0518,NC7H16:0.0055,O2:0.099,AR:0.84348'
r = ct.IdealGasReactor(gas, name='R1')
sim = ct.ReactorNet([r])

N = 20 # for top 10 most sensitive reactions
n = 606 # total number of reactions

# enable sensitivity with respect to the rates of the first 10
# reactions (reactions 0 through 9)
for i in range(n):
    r.add_sensitivity_reaction(i)

# set the tolerances for the solution and for the sensitivity coefficients
sim.rtol = 1.0e-6
sim.atol = 1.0e-15
sim.rtol_sensitivity = 1.0e-6
sim.atol_sensitivity = 1.0e-6

s_max = [0]*n
z=0
T = np.arange(0,70e-3,1e-3)
states = ct.SolutionArray(gas, extra=['time_in_ms','t']) #solution array storing all thermo variables

for t in T:
    print('Time value is: ' + str(t))
    sim.advance(t)
    for j in range(n):
        s = sim.sensitivity('OH', j)
        states.append(r.thermo.state, time_in_ms=1000*t, t=t)
        if np.abs(s)>= np.abs(s_max[j]):
            s_max[j] = s
    
s_plot = [] #for storing reaction string for 10 most sensitive reactions
s_new = np.zeros((N)) #storing the sensitivity values for 10 most sensitive reactions

for m in range(N): #loop for extracing 10 most sensitive reactions from the set of 325 reactions
	maximum = np.max(s_max)
	minimum = np.min(s_max)

	if np.abs(maximum)>np.abs(minimum):
		s_new[m]=maximum
		Reaction_index=s_max.index(maximum)
		s_plot.append(sim.sensitivity_parameter_name(Reaction_index))
		print('%0.3s %10.3f'%(s_plot,maximum))
		s_max.remove(maximum)

	else:
		s_new[m]=minimum
		Reaction_index=s_max.index(minimum)
		s_plot.append(sim.sensitivity_parameter_name(Reaction_index))
		print('%0.3s %10.3f'%(s_plot,minimum))
		s_max.remove(minimum)		

for k in range(N):
	s_plot[k] = s_plot[k].replace('IdealGasReactor','') # replacing the string using replace function

reactions = list(reversed(s_plot))
sensitivity = list(reversed(s_new))
	
plt.figure(1)
plt.barh(reactions,sensitivity)
plt.xlabel('Sensitivity value')
plt.title('20 most sensitive reactions with respect to OH radical')
plt.show()
